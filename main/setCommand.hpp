
/*
Funciones que permiten manejar los comandos enviados por el formulario web
*/

void setTime(String arg, int* h, int * m, int* s) {
  int len_arg = arg.length();
  if (len_arg == 8) {
    int indx = arg.indexOf('.');
    String hh = arg.substring(0, indx);
    String mm = arg.substring(indx + 1, 5);
    String ss = arg.substring(6, 8);

    *h = hh.toInt();
    *m = mm.toInt();
    *s = ss.toInt();

    //String hora = String("hora: " + hh + ":" + mm + ":" + ss + "\n");
    //hora hh:mm:ss\n
    rtc.setTime(*s, *m, *h, 26, 5, 2022);

    Serial.print(rtc.getTime("%A, %B %d 8%Y %H:%M:%S"));
  }
  else{
    Serial.println("argumento incorrecto");
  }
}


void command(String Cmmnd, String arg) {

  Serial.println("Comando: " + Cmmnd);
  Serial.println("Argumento: " + arg);

  if (Cmmnd == "ST") {
    setTime(arg, &hr, &mn, &sc);
  }
  else {
    Serial.println("No se reconoce comando");
  }
}

// ST hh.mm.ss
void recivCommand (String CmndArg) {
  int index = CmndArg.indexOf(' ');
  int len = CmndArg.length() + 1;

  String Cmnd = CmndArg.substring(0, index);
  String Arg = CmndArg.substring(index + 1, len);

  Serial.println("Recibido: " + CmndArg);

  command(Cmnd, Arg);
}

