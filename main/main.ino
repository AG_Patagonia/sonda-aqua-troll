/*
   Código principal para la conexion del IN-SITU/ESP32/SD

     SDI   |   Esp32
     Signal     G21
     ....................
     SD Card | ESP32
     MISO       G19
     SCK        G18
     MOSI       G23
     CS         G5
     ...................
     Diodos | ESP32
     D1       G4
     D2       G2

   Se deben mantener en la misma carpeta los archivos aux_SD.h, aux_SDI
*/

// Librerías para crear la red WiFi y el servidor HTTP
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>

// Librerías para la comunicación de la microSD
#include "FS.h"
#include "SD.h"
#include "SPI.h"

// Librería para la comunicación con la sonda Aqua TROLL 500
#include <SDI12.h>

// Librería ajuste hora ESP32
#include <ESP32Time.h>
//ESP32Time rtc;
ESP32Time rtc(3600);  // offset in seconds GMT+1

#define SERIAL_BAUD 115200  // baud rate Serial
#define DATA_PIN 21         // pin de datos del sensor 
#define POWER_PIN -1        // The sensor power pin (or -1 if not switching power)

#define D1 4      //pin al diodo 1
#define D2 2      //pin al diodo 2


SDI12 mySDI12(DATA_PIN);    //objeto para el bus de datos del sensor
File file;                  //objeto para el archivo de la SD

/*----------------------------------------------------*/
int hr = 0, mn = 0, sc = 0;  // variables para ajustar la hora
/*----------------------------------------------------*/
String SResult;       //Variable auxiliar para almacenar los datos y escribirlos en SD
/*----------------------------------------------------*/
//variables para el servidor
bool stateButton = false;
String buttonState;
const char* PARAM_INPUT_1 = "input1";
/*----------------------------------------------------*/
//variables para el sensor
bool isActive[64] = {  // realiza un seguimiento de las direcciones activas
  0,
};
uint8_t numSensors = 0;

/*----------------------------------------------------*/
//variables para los diodos laser
int ledState = HIGH;
unsigned long previousMillis = 0;
unsigned long currentMillis = 0;
/*----------------------------------------------------*/
#include "config.h"
#include "aux_SD.hpp"
#include "aux_SDI12.hpp"
#include "iniciar.hpp"
#include "diodos.hpp"
#include "setCommand.hpp"
#include "servidor.hpp"

/*----------------------------------------------------*/

void setup() {

  rtc.setTime(sc, mn, hr, 26, 5, 2022);  // Ajuste hora day-th M 2022 hr:mn:sc

  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);

  initSys(); //definido en iniciar.hpp
  initServer();  //definido en servidor.hpp
}

void loop() {
 
  while (stateButton) {
    
    currentMillis = millis();
    
    inter_d1d2(currentMillis); //definido en diodos.hpp

    SResult = "";
    String commands[] = {"", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    String tm = rtc.getTime("%H:%M:%S");

    appendFile(SD, "/datos.csv", tm.c_str());     //appendFile definido en aux_SD.hpp
    Serial.print(tm);

    for (uint8_t a = 0; a < 2; a++) {
      // measure one at a time
      for (byte i = 0; i < 62; i++) {
        char addr = decToChar(i);
        if (isActive[i]) {
          SResult = takeMeasurement(addr, commands[a]);   //takeMeasurement definido en aux_SDI12.hpp
          appendFile(SD, "/datos.csv", SResult.c_str());  
          Serial.print(","+SResult);
          Serial.println();
        }
      }
    }
    //Serial.println();
    appendFile(SD, "/datos.csv", "\n");

    delay(500L);  // wait - seconds between measurement attempts.
  }
  delay(10);

}
