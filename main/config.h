// Datos de la red -> SSID & Password
const char* ssid = "ESP32";
const char* password = "12345678"; // opcional softAP()

// Definimos la configuración de la IP local
IPAddress ip(192, 168, 4, 22);      // (192, 168, 1, 1)
IPAddress gateway(192, 168, 4, 9);  // (192, 168, 1, 1)
IPAddress subnet(255, 255, 255, 0); // (255, 255, 255, 0)
